package graceful

import (
	"os"
	"os/signal"
	"sync"
)

type Responder func(s os.Signal, done func())

var hooks = make([]Responder, 0)

// Add a function to run
func AddHook(f Responder) {
	hooks = append(hooks, f)
}

// Run functions, for example at the end of main()
func RunHooks() {
	runHooks(nil)
}

func runHooks(s os.Signal) {
	var wg sync.WaitGroup

	done := func() {
		wg.Done()
	}

	for _, f := range hooks {
		tmpF := f
		wg.Add(1)
		go func() {
			tmpF(s, done)
		}()
	}
	wg.Wait()
	os.Exit(0)
}

// Listen to OS signals
func Listen(sig ...os.Signal) {
	c := make(chan os.Signal)
	signal.Notify(c, sig...)

	go func() {
		for {
			sig := <-c
			runHooks(sig)
		}
	}()
}
